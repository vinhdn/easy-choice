package com.ath.easychoice

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.ath.easychoice.common.Constants
import com.ath.easychoice.helper.LocaleHelper
import com.ath.easychoice.helper.PrefHelper
import com.ath.easychoice.model.User
import com.ath.easychoice.views.auth.LoginActivity
import java.util.*
import com.ath.easychoice.helper.MyContextWrapper



class App: MultiDexApplication() {

    var user: User? = null

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        instance = this
    }

    fun restartApp() {
        val intent = Intent(this, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    fun changeLanguage(lang: String = Constants.Lang.vi) {
        val locale = Locale(lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.setLocale(locale)
        LocaleHelper.setLocale(this, lang)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            baseContext.createConfigurationContext(config)
            instance.baseContext.createConfigurationContext(config)
        } else {
            baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)
            instance.baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)
            instance.resources.updateConfiguration(config, instance.resources.displayMetrics)
        }
        PrefHelper.shared.putValue(Constants.Key.language, lang)
        restartApp()
    }

    fun getCurrentLang(): String {
//        return PreferencesHelper.shared.getStringValue(Constant.Key.language, Locale.getDefault().language) ?: Locale.getDefault().language
        return LocaleHelper.getLanguage(this)
    }

    fun isLangVi(): Boolean {
        return getCurrentLang() == "vi"
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(base, LocaleHelper.getLanguage(base)))
        MultiDex.install(this)
    }

    companion object {

        private lateinit var instance: App
        fun shared(): App {
            return instance
        }
    }
}