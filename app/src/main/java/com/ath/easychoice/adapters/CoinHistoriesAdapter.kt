package com.ath.easychoice.adapters

import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.RecyclerView
import com.ath.easychoice.R
import com.ath.easychoice.helper.extensions.inflate
import com.ath.easychoice.helper.extensions.visible
import com.ath.easychoice.model.TopicResult
import kotlinx.android.synthetic.main.item_coin_history.view.*
import java.lang.Exception

class CoinHistoriesAdapter(var histories: List<TopicResult>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return Holder(parent.inflate(R.layout.item_coin_history))
    }

    override fun getItemCount(): Int {
        return histories.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.apply {
            val history = histories[position]
            val split = history.result.split(",")
            result1.visible(split.size > 0)
            result2.visible(split.size > 1)
            result3.visible(split.size > 2)
            result4.visible(split.size > 3)
            result1.setImageResource(getDrawable(split, 0))
            result2.setImageResource(getDrawable(split, 1))
            result3.setImageResource(getDrawable(split, 2))
            result4.setImageResource(getDrawable(split, 3))
        }
    }

    @DrawableRes
    private fun getDrawable(split: List<String>, position: Int): Int {
        return try {
            if (split[position].toInt() == 0) {
                R.drawable.coin_item_1_1
            } else {
                R.drawable.coin_item_1_2
            }
        } catch (_ : Exception) {
            R.drawable.bg_circle_dark_gray
        }
    }

    inner class Holder(view: View): RecyclerView.ViewHolder(view)
}