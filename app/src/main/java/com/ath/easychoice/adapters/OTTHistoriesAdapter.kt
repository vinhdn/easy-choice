package com.ath.easychoice.adapters

import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.RecyclerView
import com.ath.easychoice.R
import com.ath.easychoice.helper.extensions.getHand
import com.ath.easychoice.helper.extensions.inflate
import com.ath.easychoice.model.TopicResult
import kotlinx.android.synthetic.main.item_ott_history.view.*
import java.lang.Exception

class OTTHistoriesAdapter(var histories: List<TopicResult>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return Holder(parent.inflate(R.layout.item_ott_history))
    }

    override fun getItemCount(): Int {
        return histories.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.apply {
            val history = histories[position]
            val split = history.result.split(",")
            when (isYourWin(split)) {
                0 -> {
                    tvYourResult.setText(R.string.win)
                    tvComputerResult.setText(R.string.lose)
                }
                1 -> {
                    tvYourResult.setText(R.string.lose)
                    tvComputerResult.setText(R.string.win)
                }
                else -> {
                    tvYourResult.setText(R.string.draw)
                    tvComputerResult.setText(R.string.draw)
                }
            }
            result1.setImageResource(getDrawable(split, 0))
            result2.setImageResource(getDrawable(split, 1))
        }
    }

    @DrawableRes
    private fun getDrawable(split: List<String>, position: Int): Int {
        return try {
            split[position].toInt().getHand()
        } catch (_: Exception) {
            R.drawable.bg_circle_dark_gray
        }
    }

    private fun isYourWin(split: List<String>): Int {
        try {
            val your = split[0].toInt()
            val com = split[1].toInt()
            return if (your == com) {
                2
            } else if ((your == 0 && com == 2) ||
                (your == 1 && com == 0) ||
                (your == 2 && com == 1)
            ) {
                0
            } else {
                1
            }
        } catch (_: Exception) {
            R.drawable.bg_circle_dark_gray
        }
        return 0
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view)
}