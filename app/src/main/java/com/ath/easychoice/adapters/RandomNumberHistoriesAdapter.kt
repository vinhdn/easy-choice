package com.ath.easychoice.adapters

import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.RecyclerView
import com.ath.easychoice.R
import com.ath.easychoice.helper.extensions.getDice
import com.ath.easychoice.helper.extensions.inflate
import com.ath.easychoice.helper.extensions.visible
import com.ath.easychoice.model.TopicResult
import kotlinx.android.synthetic.main.item_random_number_history.view.*
import java.lang.Exception

class RandomNumberHistoriesAdapter(var histories: List<TopicResult>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return Holder(parent.inflate(R.layout.item_random_number_history))
    }

    override fun getItemCount(): Int {
        return histories.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.apply {
            val history = histories[position]
            val split = history.result.split(",")
            result1.visible(split.size > 0)
            result2.visible(split.size > 1)
            result3.visible(split.size > 2)
            result4.visible(split.size > 3)
            if (split.isNotEmpty())
                result1.text = split[0]
            if (split.size > 1)
                result2.text = split[1]
            if (split.size > 2)
                result3.text = split[2]
            if (split.size > 3)
                result4.text = split[3]
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view)
}