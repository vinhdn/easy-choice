package com.ath.easychoice.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ath.easychoice.R
import com.ath.easychoice.helper.extensions.inflate
import com.ath.easychoice.model.TopicResult
import kotlinx.android.synthetic.main.item_roulette_history.view.*

class RouletteHistoriesAdapter(var histories: List<TopicResult>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return Holder(parent.inflate(R.layout.item_roulette_history))
    }

    override fun getItemCount(): Int {
        return histories.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.apply {
            val history = histories[position]
            tvTopicName.text = history.topicName
            tvOptionName.text = history.result
        }
    }

    inner class Holder(view: View): RecyclerView.ViewHolder(view)
}