package com.ath.easychoice.helper.animation;

import android.view.animation.Animation;

import android.view.animation.Transformation;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.widget.ImageView;

public class Rotate3dAnimation extends Animation {
    private final float fromYDegrees;
    private final float toYDegrees;
    private Camera camera;
    private int width = 0;
    private int height = 0;
    private ImageView imageView;
    private int[] resource;
    private int numOfRepetition = 0;

    public Rotate3dAnimation(ImageView imageView, int[] resource, float fromYDegrees, float toYDegrees, int numberStart) {
        this.fromYDegrees = fromYDegrees;
        this.toYDegrees = toYDegrees;
        this.imageView = imageView;
        this.resource = resource;
        this.numOfRepetition = numberStart;
    }

    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
        this.width = width / 2;
        this.height = height / 2;
        camera = new Camera();
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        float yDegrees = fromYDegrees + ((toYDegrees - fromYDegrees) * interpolatedTime);
        final Matrix matrix = t.getMatrix();
        System.err.println(interpolatedTime);
        if (interpolatedTime >= 0.5f) {
            if (interpolatedTime == 1f){
                numOfRepetition++;
            } else {
                imageView.setImageResource(resource[numOfRepetition % resource.length]);
            }
        } else if (interpolatedTime == 0) {

            imageView.setImageResource(resource[numOfRepetition % resource.length]);
        }
        camera.save();
        camera.rotateY(yDegrees);
        camera.getMatrix(matrix);
        camera.restore();

        matrix.preTranslate(-this.width, -this.height);
        matrix.postTranslate(this.width, this.height);
    }

}