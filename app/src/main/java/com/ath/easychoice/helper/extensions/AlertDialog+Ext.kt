package com.ath.easychoice.helper.extensions

import android.app.AlertDialog
import android.content.Context
import android.text.SpannableStringBuilder
import android.text.style.ClickableSpan
import android.text.style.URLSpan
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.ath.easychoice.R
import com.ath.easychoice.adapters.*
import com.ath.easychoice.common.Constants
import com.ath.easychoice.helper.PrefHelper
import com.ath.easychoice.model.TopicResult
import kotlinx.android.synthetic.main.layout_setting.view.*
import kotlinx.android.synthetic.main.layout_topic_histories.view.*
import kotlinx.android.synthetic.main.layout_setting.view.btnClose as btnSettingClose

fun showConfirm(context: Context, title: String? = null, content: String, rightButtonTitle: String = "OK", leftButtonTitle: String? = null, rightButtonClickHandler: (() -> Unit)? = null, cancelListener: (() -> Unit)? = null) {
    val dialog = AlertDialog.Builder(context)
    dialog.setCancelable(true)
    dialog.setPositiveButton(rightButtonTitle) { dia, _ ->
        dia.dismiss()
        rightButtonClickHandler?.invoke()
    }
    title?.let {
        dialog.setTitle(title)
    }
    leftButtonTitle?.let {
        dialog.setNegativeButton(it) { dia, _ ->
            dia.dismiss()
        }
    }
    dialog.setOnCancelListener {
        cancelListener?.invoke()
    }
    dialog.setOnDismissListener {
        cancelListener?.invoke()
    }
    dialog.setMessage(content)
    dialog.create().show()
}


fun URLSpan.makeLinkClickable(strBuilder: SpannableStringBuilder, click: () -> Unit) {
    val start = strBuilder.getSpanStart(this)
    val end = strBuilder.getSpanEnd(this)
    val flags = strBuilder.getSpanFlags(this)
    val clickable = object : ClickableSpan() {
        override fun onClick(view: View) {
            click.invoke()
        }
    }
    strBuilder.setSpan(clickable, start, end, flags)
    strBuilder.removeSpan(this)
}

fun <T> showSelection(context: Context, title: String? = null, listType: List<T>, selectedListener: (T) -> Unit){
    val dialog = AlertDialog.Builder(context)
    dialog.setCancelable(true)
    dialog.setPositiveButton(R.string.cancel) { dia, _ ->
        dia.dismiss()
    }
    title?.let {
        dialog.setTitle(title)
    }

    val arrayAdapter = ArrayAdapter<T>(context, android.R.layout.select_dialog_item)
    arrayAdapter.addAll(listType)
    dialog.setAdapter(arrayAdapter) { dialogI, position ->
        selectedListener.invoke(listType[position])
        dialogI.dismiss()
    }
    dialog.create().show()
}

fun showRouletteHistories(context: Context, listHistories: List<TopicResult>){
    val builder = AlertDialog.Builder(context)
    builder.setCancelable(true)
    builder.setTitle(null)
    var dialog: AlertDialog? = null
    val view = LayoutInflater.from(context).inflate(R.layout.layout_topic_histories, null, false)
    view.apply {
        rvHistories.layoutManager = LinearLayoutManager(context)
        rvHistories.adapter = RouletteHistoriesAdapter(listHistories)
        btnClose.click {
            dialog?.dismiss()
        }
    }
    builder.setView(view)
    dialog = builder.create()
    dialog.show()
}

fun showCoinHistories(context: Context, listHistories: List<TopicResult>){
    val builder = AlertDialog.Builder(context)
    builder.setCancelable(true)
    builder.setTitle(null)
    var dialog: AlertDialog? = null
    val view = LayoutInflater.from(context).inflate(R.layout.layout_topic_histories, null, false)
    view.apply {
        rvHistories.layoutManager = LinearLayoutManager(context)
        rvHistories.adapter = CoinHistoriesAdapter(listHistories)
        btnClose.click {
            dialog?.dismiss()
        }
    }
    builder.setView(view)
    dialog = builder.create()
    dialog.show()
}

fun showDiceHistories(context: Context, listHistories: List<TopicResult>){
    val builder = AlertDialog.Builder(context)
    builder.setCancelable(true)
    builder.setTitle(null)
    var dialog: AlertDialog? = null
    val view = LayoutInflater.from(context).inflate(R.layout.layout_topic_histories, null, false)
    view.apply {
        rvHistories.layoutManager = LinearLayoutManager(context)
        rvHistories.adapter = DiceHistoriesAdapter(listHistories)
        btnClose.click {
            dialog?.dismiss()
        }
    }
    builder.setView(view)
    dialog = builder.create()
    dialog.show()
}

fun showRandomNumberHistories(context: Context, listHistories: List<TopicResult>){
    val builder = AlertDialog.Builder(context)
    builder.setCancelable(true)
    builder.setTitle(null)
    var dialog: AlertDialog? = null
    val view = LayoutInflater.from(context).inflate(R.layout.layout_topic_histories, null, false)
    view.apply {
        rvHistories.layoutManager = LinearLayoutManager(context)
        rvHistories.adapter = RandomNumberHistoriesAdapter(listHistories)
        btnClose.click {
            dialog?.dismiss()
        }
    }
    builder.setView(view)
    dialog = builder.create()
    dialog.show()
}

fun showOTTHistories(context: Context, listHistories: List<TopicResult>){
    val builder = AlertDialog.Builder(context)
    builder.setCancelable(true)
    builder.setTitle(null)
    var dialog: AlertDialog? = null
    val view = LayoutInflater.from(context).inflate(R.layout.layout_topic_histories, null, false)
    view.apply {
        rvHistories.layoutManager = LinearLayoutManager(context)
        rvHistories.adapter = OTTHistoriesAdapter(listHistories)
        btnClose.click {
            dialog?.dismiss()
        }
    }
    builder.setView(view)
    dialog = builder.create()
    dialog.show()
}

fun showSetting(context: Context, applyView: (View)-> Unit){
    val builder = AlertDialog.Builder(context)
    builder.setCancelable(true)
    builder.setTitle(null)
    var dialog: AlertDialog? = null
    val view = LayoutInflater.from(context).inflate(R.layout.layout_setting, null, false)
    view.apply {
        btnSettingClose.click {
            dialog?.dismiss()
        }
        applyView(this)
    }
    builder.setView(view)
    dialog = builder.create()
    dialog.show()
}

fun showSelectionPartOfDay(context: Context, title: String? = null, listString: List<String>, selectedListener: (Int) -> Unit) {
    val dialog = AlertDialog.Builder(context)
    dialog.setCancelable(true)
    dialog.setPositiveButton(R.string.cancel) { dia, _ ->
        dia.dismiss()
    }
    title?.let {
        dialog.setTitle(title)
    }

    val arrayAdapter = ArrayAdapter<String>(context, android.R.layout.select_dialog_item)
    listString.forEach {
        arrayAdapter.addAll(it)
    }
    dialog.setAdapter(arrayAdapter) { dialogI, position ->
        selectedListener(position)
        dialogI.dismiss()
    }
    dialog.create().show()
}
