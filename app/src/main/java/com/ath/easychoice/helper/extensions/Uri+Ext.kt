package com.ath.easychoice.helper.extensions

import android.net.Uri
import android.provider.MediaStore
import com.ath.easychoice.App
import java.io.File

fun Uri.filename(): Pair<String?, String?> {
    var ret: String? = null
    var mime: String? = null

    if (scheme == "content") {
        val cursor = App.shared().contentResolver.query(this, arrayOf(MediaStore.MediaColumns.DISPLAY_NAME,
                MediaStore.MediaColumns.MIME_TYPE), null, null, null)
        if (cursor != null) {
            cursor.moveToFirst()
            ret = cursor.getString(0)
            mime = cursor.getString(1)
            cursor.close()
        }
    } else if (scheme == "file") {
        val file = File(path)
        ret = file.name
        mime = file.extension
    }

    return ret to mime
}