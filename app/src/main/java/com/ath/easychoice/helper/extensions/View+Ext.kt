package com.ath.easychoice.helper.extensions

import android.animation.*
import android.app.Activity
import android.graphics.Color
import android.view.animation.AlphaAnimation
import android.widget.EditText
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.RoundRectShape
import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.View.LAYER_TYPE_SOFTWARE
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import androidx.annotation.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import com.ath.easychoice.R
import com.ath.easychoice.helper.KeyboardHelper
import com.ath.easychoice.helper.animation.Rotate3dAnimation
import com.ath.easychoice.views.base.BaseActivity
import com.ath.easychoice.views.base.BaseFragment
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject


fun View.visible(isVisible: Boolean = true) {
    if (isVisible)
        visibility = View.VISIBLE
    else
        visibility = View.GONE
}

fun View.gone() {
    visibility = View.GONE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}
fun View.invisible(isInvisible: Boolean = true) {
    visibility = if (isInvisible) View.INVISIBLE else View.VISIBLE
}

fun View.isVisble(): Boolean = this.visibility == View.VISIBLE

@UiThread
inline fun View.fadeOut() {
    fadeOut(500)
}

@UiThread
inline fun View.fadeIn() {
    fadeIn(500)
}

@UiThread
inline fun View.fadeIn(duration: Long) {
    this.clearAnimation()
    val anim = AlphaAnimation(this.alpha, 1.0f)
    anim.duration = duration
    this.startAnimation(anim)
}

@UiThread
inline fun View.fadeOut(duration: Long) {
    this.clearAnimation()
    val anim = AlphaAnimation(this.alpha, 0.0f)
    anim.duration = duration
    this.startAnimation(anim)
}

fun View.addChild(view: View) {
    if (this is ViewGroup) {
        this.addView(view)
    }
}


fun View.setupHiddenKeyboard(activity: Activity) {
    // Set up touch listener for non-text box views to hide keyboard.
    if (this !is EditText) {
        this.setOnTouchListener { _, motion ->
            if (motion.action == MotionEvent.ACTION_UP && (motion.eventTime - motion.downTime) <= 200) {
                KeyboardHelper.shared.hideSoftKeyboard(activity)
                false
            } else {
//                motion.action == MotionEvent.ACTION_UP
                false
            }
        }
    }

    //If a layout container, iterate over children and seed recursion.
    if (this is ViewGroup) {
        for (i in 0 until this.childCount) {
            val innerView = this.getChildAt(i)
            innerView.setupHiddenKeyboard(activity)
        }
    }
}

fun EditText.showETError(error: String) {
    this.error = error
    this.requestFocus()
}

fun View.goneBottomToTop() {
    this.animate()
            .translationY(0f - this.height.toFloat())
            .alpha(0.0f)
            .setDuration(300)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    gone()
                }
            })
}

fun View.visibleTopToBottom() {
    visible()
    this.animate()
            .translationY(0f)
            .alpha(1.0f)
            .setDuration(300)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    visible()
                }
            })
}

fun View.visibleSlideFromTop() {
    this.animate().translationY(-this.height.toFloat())
            .setDuration(0)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    visible()
                    animate().translationY(0.0f).duration = 300
                }
            })
}

fun View.hideSlideToTop() {
    this.animate().translationY(-this.height.toFloat())
            .setDuration(300)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    gone()
                }
            })
}


fun <T : View?> AppCompatActivity.bindBy(@IdRes idRes: Int): Lazy<T>? {
    return unsafeLazy { findViewById<T>(idRes) }
}

fun <T : View?> View.bindBy(@IdRes idRes: Int): Lazy<T>? {
    @Suppress("UNCHECKED_CAST")
    return unsafeLazy { findViewById<T>(idRes) }
}

fun <T : View> AppCompatActivity.bind(@IdRes idRes: Int): T? {
    return findViewById(idRes)
}

fun <T : View> View.bind(@IdRes idRes: Int): T? {
    return findViewById(idRes)
}

private fun <T> unsafeLazy(initializer: () -> T) = lazy(LazyThreadSafetyMode.NONE, initializer)

//fun RecyclerView.getColumn(itemWidth: Int): Int {
//    val displayMetrics = context.resources.displayMetrics
//    val dpWidth = displayMetrics.widthPixels / displayMetrics.density
//    return (dpWidth / itemWidth).toInt()
//}

fun View.setBackgroundWithShadow(@ColorRes backgroundColor: Int = R.color.white,
                                 @DimenRes cornerRadius: Int = R.dimen.radius_small,
                                 @ColorRes shadowColor: Int = R.color.grayLine,
                                 @DimenRes elevation: Int = R.dimen.elevation,
                                 shadowGravity: Int = Gravity.TOP) {
    val cornerRadiusValue = context.resources.getDimension(cornerRadius)
    val elevationValue = context.resources.getDimension(elevation).toInt()
    val shadowColorValue = ContextCompat.getColor(context, shadowColor)
    val backgroundColorValue = ContextCompat.getColor(context, backgroundColor)

    val outerRadius = floatArrayOf(cornerRadiusValue, cornerRadiusValue, cornerRadiusValue, cornerRadiusValue, cornerRadiusValue, cornerRadiusValue, cornerRadiusValue, cornerRadiusValue)

    val backgroundPaint = Paint()
    backgroundPaint.style = Paint.Style.FILL
    backgroundPaint.setShadowLayer(cornerRadiusValue, 0f, 0f, 0)

    val shapeDrawablePadding = Rect()
    shapeDrawablePadding.left = elevationValue
    shapeDrawablePadding.right = elevationValue

    val DY: Int
    when (shadowGravity) {
        Gravity.CENTER -> {
            shapeDrawablePadding.top = elevationValue
            shapeDrawablePadding.bottom = elevationValue
            DY = 0
        }
        Gravity.TOP -> {
            shapeDrawablePadding.top = elevationValue * 2
            shapeDrawablePadding.bottom = elevationValue
            DY = -1 * elevationValue / 3
        }
        Gravity.BOTTOM -> {
            shapeDrawablePadding.top = elevationValue
            shapeDrawablePadding.bottom = elevationValue * 2
            DY = elevationValue / 3
        }
        else -> {
            shapeDrawablePadding.top = elevationValue
            shapeDrawablePadding.bottom = elevationValue * 2
            DY = elevationValue / 3
        }
    }

    val shapeDrawable = ShapeDrawable()
    shapeDrawable.setPadding(shapeDrawablePadding)

    shapeDrawable.paint.color = backgroundColorValue
    shapeDrawable.paint.setShadowLayer(cornerRadiusValue / 3, 0f, DY.toFloat(), shadowColorValue)

    setLayerType(LAYER_TYPE_SOFTWARE, shapeDrawable.paint)

    shapeDrawable.shape = RoundRectShape(outerRadius, null, null)

    val drawable = LayerDrawable(arrayOf<Drawable>(shapeDrawable))
    drawable.setLayerInset(0, elevationValue, elevationValue * 2, elevationValue, elevationValue * 2)

    background = drawable
}

fun List<View>.click(clickListener: (View) -> Unit) {
    this.forEach {
        it.click(clickListener)
    }
}

fun EditText.textChanges(): Observable<Pair<String, View>> {
    val subject = PublishSubject.create<Pair<String, View>>()
    return subject.doOnSubscribe {
        this.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                subject.onNext(s.toString() to this@textChanges)
            }
        })
    }
}

fun View.zoomInOut(minScale: Float = 1.0f, maxScale: Float = 2.0f) {
    val scaleX = ObjectAnimator.ofFloat(this, "scaleX", minScale, maxScale)
    val scaleY = ObjectAnimator.ofFloat(this, "scaleY", minScale, maxScale)

    val scaleAnim = AnimatorSet()
    scaleAnim.duration = 4000
    scaleX.repeatCount = ValueAnimator.INFINITE
    scaleX.repeatMode = ValueAnimator.REVERSE
    scaleX.duration = 2000
    scaleY.repeatCount = ValueAnimator.INFINITE
    scaleY.repeatMode = ValueAnimator.REVERSE
    scaleY.duration = 2000
    scaleAnim.play(scaleX).with(scaleY)
    scaleAnim.interpolator = AccelerateDecelerateInterpolator()
    scaleAnim.start()
}

fun ImageView.replaceAnimation(startResource: Int, endResource: Int, duration: Int = 4000, delay: Int = 0) {
    val scaleX = ObjectAnimator.ofFloat(this, "scaleX", 1f, 1.0f)
    val scaleY = ObjectAnimator.ofFloat(this, "scaleY", 1f, 1.0f)
    val scaleAnim = AnimatorSet()
    scaleAnim.duration = duration.toLong()
    scaleX.repeatCount = ValueAnimator.INFINITE
    scaleX.repeatMode = ValueAnimator.REVERSE
    scaleX.duration = duration.toLong()
    scaleY.repeatCount = ValueAnimator.INFINITE
    scaleY.repeatMode = ValueAnimator.REVERSE
    scaleY.duration = duration.toLong()
    scaleX.startDelay = delay.toLong()
    scaleAnim.play(scaleX)
    scaleAnim.startDelay = delay.toLong()
    scaleAnim.interpolator = AccelerateDecelerateInterpolator()
    var repeatCount = 0
//    scaleX.addListener()
    scaleX.addListener(object : Animator.AnimatorListener {
        override fun onAnimationEnd(animation: Animator?) {

        }

        override fun onAnimationCancel(animation: Animator?) {

        }

        override fun onAnimationStart(animation: Animator?) {

        }

        override fun onAnimationRepeat(animation: Animator?) {
            repeatCount++
            if (repeatCount % 2 == 1) {
                setImageResource(endResource)
            } else {
                setImageResource(startResource)
            }
        }

    })
    scaleAnim.start()
}

fun View.click(listener: (View) -> Unit) {
    isSoundEffectsEnabled = false
    setOnClickListener {
        if (context is BaseActivity) {
            (context as? BaseActivity)?.playAudio(R.raw.click)
        }
        listener(it)
    }
    setOnTouchListener { view, event ->
        val action = event?.action
        if (action == MotionEvent.ACTION_DOWN) {
            view.animate().scaleX(0.9f).setDuration(100).start()
            view.animate().scaleY(0.9f).setDuration(100).start()
            return@setOnTouchListener false
        } else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL) {
            view.animate().cancel()
            view.animate().scaleX(1f).setDuration(100).start()
            view.animate().scaleY(1f).setDuration(100).start()
            return@setOnTouchListener false
        }
        return@setOnTouchListener false
    }
}

fun Activity.setLightStatusBar() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        var flags = window.decorView.systemUiVisibility
        flags = flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        window.decorView.systemUiVisibility = flags
        window.statusBarColor = Color.WHITE
    }
}

fun Activity.clearLightStatusBar(@ColorInt color: Int = R.color.colorPrimary.getColor()) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        var flags = window.decorView.systemUiVisibility
        flags = flags and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
        window.decorView.systemUiVisibility = flags
        window.statusBarColor = color
    }
}

fun View.isVisible(): Boolean = this.visibility == View.VISIBLE

fun ImageView.rotate3D(@DrawableRes start: Int, @DrawableRes end: Int, resultStart: Boolean = true) {
    val animation =
        Rotate3dAnimation(this, intArrayOf(start, end), 180f, 0f, if (resultStart) 0 else 1)
    animation.repeatCount = 10
    animation.duration = 220
    animation.interpolator = LinearInterpolator()
    startAnimation(animation)
}

fun ImageView.rotateDice(resultStart: Int = 2) {
    val animation =
        Rotate3dAnimation(this, intArrayOf(R.drawable.ic_dice_2_points, R.drawable.ic_dice_4_points, R.drawable.ic_dice_6_points), 180f, 0f, resultStart / 2 - 1)
    animation.repeatCount = 10
    animation.duration = 220
    animation.interpolator = LinearInterpolator()
    startAnimation(animation)
}


