package com.ath.easychoice.model

import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

data class OptionModel(var id: String = "",
                       var name: String = "",
                       var nameEn: String = ""): Parcelable, Serializable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    )

    override fun toString(): String {
        return name
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(nameEn)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OptionModel> {
        override fun createFromParcel(parcel: Parcel): OptionModel {
            return OptionModel(parcel)
        }

        override fun newArray(size: Int): Array<OptionModel?> {
            return arrayOfNulls(size)
        }
    }
}