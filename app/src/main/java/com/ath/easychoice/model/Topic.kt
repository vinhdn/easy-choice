package com.ath.easychoice.model

import com.google.firebase.Timestamp
import java.io.Serializable

data class Topic(
    var id: String = "",
    var name: String = "",
    var nameEn: String = "",
    var userId: String = "",
    var likes: Int = 0,
    var isShared: Boolean = false,
    var options: ArrayList<OptionModel> = arrayListOf(),
    var timeUpdate: Timestamp = Timestamp.now()
): Serializable