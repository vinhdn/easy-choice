package com.ath.easychoice.model

import com.google.firebase.Timestamp

data class TopicResult(
    var result: String = "",
    var topicId: String = "",
    var userId: String = "",
    var topicName: String = "",
    var datetime: Timestamp = Timestamp.now(),
    var isPro: Boolean = false
)