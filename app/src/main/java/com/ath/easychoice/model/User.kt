package com.ath.easychoice.model

data class User(var userId: String = "",
                var fullName: String = "",
                var fbId: String = "",
                var linkAvatar: String = "",
                var isInitData: Boolean = false)