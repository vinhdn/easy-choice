package com.ath.easychoice.model.eventbus

import com.ath.easychoice.model.Topic

data class EventSelectedTopic(var topic: Topic)