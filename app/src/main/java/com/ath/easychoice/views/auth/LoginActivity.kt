package com.ath.easychoice.views.auth

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.ath.easychoice.App
import com.ath.easychoice.BuildConfig
import com.ath.easychoice.R
import com.ath.easychoice.helper.extensions.click
import com.ath.easychoice.helper.extensions.gone
import com.ath.easychoice.helper.extensions.visible
import com.ath.easychoice.model.OptionModel
import com.ath.easychoice.model.User
import com.ath.easychoice.views.base.BaseActivity
import com.ath.easychoice.views.home.MainActivity
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.SetOptions
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    val TAG = "LoginActivity"
    private lateinit var callbackManager: CallbackManager
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        tvVersion.text = "version ${BuildConfig.VERSION_NAME}"
        auth = FirebaseAuth.getInstance()
        splash.postDelayed({
            val currentUser = auth.currentUser
            if (currentUser != null) {
                getUsers()
                return@postDelayed
            } else {
                auth.signInAnonymously().addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        getUsers()
                    } else {
                        setupFb()
                    }
                }
            }
        }, 200)
    }

    private fun setupFb() {
        splash.gone()
        callbackManager = CallbackManager.Factory.create()

        btnLoginFb.visible()
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Log.d(TAG, "facebook:onSuccess:$loginResult")
                handleFacebookAccessToken(loginResult.accessToken)
            }

            override fun onCancel() {
                Log.d(TAG, "facebook:onCancel")
            }

            override fun onError(error: FacebookException) {
                Log.d(TAG, "facebook:onError", error)
            }
        })
        btnLoginFb.click {
            LoginManager.getInstance().logIn(this, listOf("public_profile"))
        }
        btnFinish.click {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
        listOf(ivCheckEng, ivCheckViet, ivCheckJapan).forEach {
            it.gone()
        }
        val currentLang = App.shared().getCurrentLang()
        when(currentLang) {
            "ja" -> {
                ivCheckJapan.visible()
            }
            "en" -> {
                ivCheckEng.visible()
            }
            else -> {
                ivCheckViet.visible()
            }
        }
        btnViet.click {
            App.shared().changeLanguage("vi")
        }
        btnEnglish.click {
            App.shared().changeLanguage("en")
        }
        btnJapan.click {
            App.shared().changeLanguage("ja")
        }
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        Log.d(TAG, "handleFacebookAccessToken:$token")

        val credential = FacebookAuthProvider.getCredential(token.token)
        if (auth.currentUser != null && auth.currentUser!!.isAnonymous) {
            auth.currentUser!!.linkWithCredential(credential).addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")
                    getUsers()
                } else {
                    auth.signInWithCredential(credential).addOnCompleteListener(this) { task1 ->
                        if (task1.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success")
                            getUsers()
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task1.exception)
                            Toast.makeText(
                                baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }
        } else
            auth.signInWithCredential(credential)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCredential:success")
                        getUsers()
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithCredential:failure", task.exception)
                        Toast.makeText(
                            baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                        hideLoading()
                    }
                }
    }

    private fun getUsers() {
        if (auth.currentUser == null) return
        db.collection("users")
            .document(auth.currentUser?.uid!!)
            .get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    // Document found in the offline cache
                    val document = task.result
                    val user = document?.toObject(User::class.java)
                    if (user == null) {
                        val profile = Profile.getCurrentProfile()
                        val fullName = if (profile != null)
                            "${profile.firstName} - ${profile.lastName}" else ""
                        val createUser = User(
                            auth.currentUser?.uid!!,
                            fullName,
                            profile?.id ?: "",
                            profile?.getProfilePictureUri(200, 200)?.toString() ?: ""
                        )
                        db.collection("users").document(auth.currentUser?.uid!!).set(createUser).addOnCompleteListener {
                            App.shared().user = createUser
                            initData()
                        }
                        return@addOnCompleteListener
                    }
                    App.shared().user = user
                    if (user.fbId.isNotEmpty()) {
                        val profile = Profile.getCurrentProfile()
                        val fullName = if (profile != null)
                            "${profile.firstName} - ${profile.lastName}" else ""
                        App.shared().user?.linkAvatar = (profile?.getProfilePictureUri(200, 200)?.toString() ?: "")
                        App.shared().user?.fullName = fullName
                        db.collection("users").document(auth.currentUser?.uid!!).update(
                            mapOf(
                                "fullName" to fullName,
                                "linkAvatar" to (profile?.getProfilePictureUri(200, 200)?.toString() ?: "")
                            )
                        ).addOnCompleteListener {
                            initData()
                        }
                    } else {
                        setupFb()
                    }
                } else {
                    setupFb()
                }
            }
    }

    public override fun onStart() {
        super.onStart()
    }

    private fun initData() {
        if (auth.currentUser == null) return
        if (App.shared().user?.isInitData == true) {
            if (App.shared().user?.fbId?.isNotEmpty() == true) {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                setupFb()
            }
            return
        }
        val topic1 = hashMapOf(
            "isShared" to false,
            "likes" to 0,
            "name" to "Hôm nay ăn gì?",
            "nameEn" to "What is eat today?",
            "userId" to auth.currentUser?.uid
        )
        topic1["options"] = listOf(
            OptionModel("123", "Buffet", "Buffet"),
            OptionModel("123", "Hải sản", "Seafood"),
            OptionModel("123", "Đồ Tầu", "Chinese food"),
            OptionModel("123", "Bít tết", "Beefsteak"),
            OptionModel("123", "Pizza", "Pizza"),
            OptionModel("123", "Mỳ Ý", "Spaghetti")
        )
        db.collection("topics").document("${auth.currentUser?.uid}001").set(topic1, SetOptions.merge())
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {

                }
            }
        val topic2 = hashMapOf(
            "isShared" to false,
            "likes" to 0,
            "name" to "1 2 3 dô",
            "nameEn" to "1 2 3 Cheer",
            "userId" to auth.currentUser?.uid
        )
        topic2["options"] = listOf(
            OptionModel("123", "1 ly", "1 glass"),
            OptionModel("123", "2 ly", "2 glass"),
            OptionModel("123", "3 ly", "3 glass"),
            OptionModel("123", "Uống tất, trừ người quay", "All, except you"),
            OptionModel("123", "Uống tất", "All"),
            OptionModel("123", "Chỉ định", "Point")
        )
        db.collection("topics").document("${auth.currentUser?.uid}002").set(topic2, SetOptions.merge())
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {

                }
            }
        val topic3 = hashMapOf(
            "isShared" to false,
            "likes" to 0,
            "name" to "Có hay Không?",
            "nameEn" to "Yes or No?",
            "userId" to auth.currentUser?.uid
        )
        topic3["options"] = listOf(
            OptionModel("123", "Có", "Yes"),
            OptionModel("123", "Không", "No")
        )
        db.collection("topics").document("${auth.currentUser?.uid}003").set(topic3, SetOptions.merge())
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {

                }
            }
        db.collection("users").document(auth.currentUser?.uid!!).update("isInitData", true)
            .addOnCompleteListener {
                App.shared().user?.isInitData = true
                if (App.shared().user?.fbId?.isNotEmpty() == true) {
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    setupFb()
                }
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Pass the activity result back to the Facebook SDK
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }
}