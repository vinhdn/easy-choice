package com.ath.easychoice.views.base

import android.content.Context
import android.content.res.AssetFileDescriptor
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RawRes
import androidx.appcompat.app.AppCompatActivity
import com.ath.easychoice.R
import com.ath.easychoice.common.Constants
import com.ath.easychoice.helper.LocaleHelper
import com.ath.easychoice.helper.PrefHelper
import com.ath.easychoice.helper.extensions.gone
import com.ath.easychoice.helper.extensions.visible
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.loading_view.view.*

abstract class BaseActivity: AppCompatActivity() {

    open lateinit var db: FirebaseFirestore
    private var mediaPlayer: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        db = FirebaseFirestore.getInstance()
    }

    fun popBackStack(name: String, flags: Int) {
        supportFragmentManager.popBackStack(name, flags)
    }

    fun popBackStackImmediate(name: String, flags: Int): Boolean {
        return supportFragmentManager.popBackStackImmediate(name, flags)
    }

    protected fun showLoading() {
        findViewById<View>(R.id.loadingView)?.visible()
    }

    protected fun hideLoading() {
        findViewById<View>(R.id.loadingView)?.gone()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase))
    }

     fun playAudio(@RawRes audioId: Int, isLoop: Boolean = false) {
         if (!PrefHelper.shared.getBooleanValue(Constants.Key.enableSound, true)) {
             return
         }
        stopAudio()
         try {
             mediaPlayer = MediaPlayer.create(this, audioId)
             mediaPlayer?.isLooping = isLoop
             mediaPlayer?.start()
         } catch (ex: Exception) {
             ex.printStackTrace()
         }
    }

     fun stopAudio() {
        mediaPlayer?.stop()
    }

    override fun onDestroy() {
        stopAudio()
        super.onDestroy()
    }
}