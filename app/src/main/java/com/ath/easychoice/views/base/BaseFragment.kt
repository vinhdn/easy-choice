package com.ath.easychoice.views.base

import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.RawRes
import androidx.fragment.app.Fragment
import com.ath.easychoice.R
import com.ath.easychoice.helper.extensions.gone
import com.ath.easychoice.helper.extensions.visible
import com.ath.easychoice.views.home.MainActivity
import com.google.firebase.firestore.FirebaseFirestore

open class BaseFragment: Fragment() {

    open lateinit var mActivity: BaseActivity
    open lateinit var db: FirebaseFirestore
    private var mediaPlayer: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (activity !is BaseActivity) {
            Throwable("Activity no override BaseActivity")
        }
        mActivity = activity as BaseActivity
        db = FirebaseFirestore.getInstance()
    }

    open fun onRefresh() {}

    fun backStack(): Boolean {
        val canBackStack = canBackFragment()
        if (canBackStack) {
            childFragmentManager.popBackStack()
        }
        return canBackStack
    }

    fun canBackFragment(): Boolean {
        return childFragmentManager.backStackEntryCount > 1
    }

    fun back() {
        try {
            if (activity is MainActivity) {
                (activity as MainActivity).backFragment()
            } else
                activity?.supportFragmentManager?.popBackStack()
        }catch (ex: IllegalStateException) {

        }
        hideKeyboard()
    }

    fun hideKeyboard() {
        val view = activity?.currentFocus
        if (view != null) {
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun popBackStack(name: String, flags: Int, isFromActivity: Boolean = true) {
        if (isFromActivity) {
            mActivity.popBackStack(name, flags)
        } else {
            childFragmentManager.popBackStack(name, flags)
        }
    }

    fun popBackStackImmediate(name: String, flags: Int, isFromActivity: Boolean = true): Boolean {
        if (isFromActivity) {
            return mActivity.popBackStackImmediate(name, flags)
        } else {
            return childFragmentManager.popBackStackImmediate(name, flags)
        }
    }

    fun showLoading() {
        view?.findViewById<View>(R.id.loadingView)?.visible()
    }

    fun hideLoading() {
        view?.findViewById<View>(R.id.loadingView)?.gone()
    }

    fun playAudio(@RawRes audioId: Int, isLoop: Boolean = false) {
        stopAudio()
        try {
            mediaPlayer = MediaPlayer.create(context, audioId)
            mediaPlayer?.isLooping = isLoop
            mediaPlayer?.start()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    fun stopAudio() {
        mediaPlayer?.stop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        stopAudio()
    }
}