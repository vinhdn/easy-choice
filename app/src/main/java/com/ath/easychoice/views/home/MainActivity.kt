package com.ath.easychoice.views.home

import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import com.ath.easychoice.App
import com.ath.easychoice.R
import com.ath.easychoice.common.Constants
import com.ath.easychoice.helper.PrefHelper
import com.ath.easychoice.helper.extensions.click
import com.ath.easychoice.helper.extensions.getColor
import com.ath.easychoice.helper.extensions.showSetting
import com.ath.easychoice.views.base.BaseActivity
import com.ath.easychoice.views.base.BaseFragment
import com.ath.easychoice.views.coin.CoinFragment
import com.ath.easychoice.views.dice.DiceFragment
import com.ath.easychoice.views.number.RandomNumberFragment
import com.ath.easychoice.views.onetwothree.OneTwoThreeFragment
import com.ath.easychoice.views.roulette.RouletteFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_setting.view.*

class MainActivity : BaseActivity() {

    companion object {
        private const val rouletteTab = "rouletteTab"
        private const val diceTab = "diceTab"
        private const val coinTab = "coinTab"
        private const val numberTab = "numberTab"
        private const val onettTab = "onettTab"

        private const val tab1Coin = coinTab
        private const val tab2Dice = diceTab
        private const val tab3Roulette = rouletteTab
        private const val tab4Number = numberTab
        private const val tab5OneTT = onettTab
    }

    private var isStart = false

    var currentTab = rouletteTab

    var tab1Fragment: BaseFragment? = null
    var tab2Fragment: BaseFragment? = null
    var tab3Fragment: BaseFragment? = null
    var tab4Fragment: BaseFragment? = null
    var tab5Fragment: BaseFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        listOf(llCoins, llDice, llRoulette, llNumber, llOTT).click {
            when (it.id) {
                R.id.llCoins -> {
                    switchTab(tab1Coin)
                }
                R.id.llDice -> {
                    switchTab(tab2Dice)
                }
                R.id.llRoulette -> {
                    switchTab(tab3Roulette)
                }
                R.id.llNumber -> {
                    switchTab(tab4Number)
                }
                R.id.llOTT -> {
                    switchTab(tab5OneTT)
                }
            }
        }
        if (savedInstanceState != null) {
            revertFragment(savedInstanceState)
        } else {
            setupTab()
        }
        btnSetting.click {
            showSetting(this) {
                it.apply {
                    swithVolume.isChecked = PrefHelper.shared.getBooleanValue(Constants.Key.enableSound, true)
                    swithVolume.setOnCheckedChangeListener { _, isChecked ->
                        PrefHelper.shared.putValue(Constants.Key.enableSound, isChecked)
                    }
                    swithVibrate.isChecked = PrefHelper.shared.getBooleanValue(Constants.Key.enableVibrate, true)
                    swithVibrate.setOnCheckedChangeListener { _, isChecked ->
                        PrefHelper.shared.putValue(Constants.Key.enableVibrate, isChecked)
                    }
                    btnShare.click {

                    }
                    btnRate.click {

                    }
                    btnTerm.click {

                    }
                    listOf(btn12m, btn6m, btn3m).click {

                    }
                    if (App.shared().user?.fbId?.isNotEmpty() == true) {
                        tvConnectFb.setText(R.string.connected)
                    } else {
                        tvConnectFb.setText(R.string.connect)
                        btnConnectFacebook.click {

                        }
                    }
                }
            }
        }
    }

    private fun setupTab() {
        switchTab(tab3Roulette)
    }

    fun switchTab(tab: String) {
        reloadTab(tab)
        val transaction = supportFragmentManager.beginTransaction()
        var fragment = supportFragmentManager.findFragmentByTag(tab) as? BaseFragment?
        var isCreated = true
        when (tab) {
            tab3Roulette -> {
                if (tab3Fragment == null) {
                    tab3Fragment = RouletteFragment()
                    isCreated = false
                }
                fragment = tab3Fragment
            }
            tab2Dice -> {
                if (tab2Fragment == null) {
                    tab2Fragment = DiceFragment()
                    isCreated = false
                }
                fragment = tab2Fragment
            }
            tab1Coin -> {
                if (tab1Fragment == null) {
                    tab1Fragment = CoinFragment()
                    isCreated = false
                }
                fragment = tab1Fragment
            }
            tab4Number -> {
                if (tab4Fragment == null) {
                    tab4Fragment = RandomNumberFragment()
                    isCreated = false
                }
                fragment = tab4Fragment
            }
            tab5OneTT -> {
                if (tab5Fragment == null) {
                    tab5Fragment = OneTwoThreeFragment()
                    isCreated = false
                }
                fragment = tab5Fragment
            }
        }
        if (fragment?.isAdded != true) {
            transaction.add(R.id.flLayout, fragment!!, tab)
        } else {
            transaction.show(fragment)
        }
        currentTab = tab
        setTabColor(tab)
        transaction.commitAllowingStateLoss()
        hiddenAllFragmentNotShow(transaction)
        if (isCreated) {
            fragment.onRefresh()
        }
    }

    private fun hiddenAllFragmentNotShow(fragmentTransaction: FragmentTransaction) {
        if (currentTab != tab1Coin) {
            tab1Fragment?.let {
                fragmentTransaction.hide(it)
            }
        }
        if (currentTab != tab2Dice) {
            tab2Fragment?.let {
                fragmentTransaction.hide(it)
            }
        }
        if (currentTab != tab3Roulette) {
            tab3Fragment?.let {
                fragmentTransaction.hide(it)
            }
        }
        if (currentTab != tab4Number) {
            tab4Fragment?.let {
                fragmentTransaction.hide(it)
            }
        }
        if (currentTab != tab5OneTT) {
            tab5Fragment?.let {
                fragmentTransaction.hide(it)
            }
        }
    }

    private fun reloadTab(tab: String) {
        when (tab) {
            tab1Coin -> {
                tab1Fragment?.onRefresh()
            }
            tab2Dice -> {
                tab2Fragment?.onRefresh()
            }
            tab3Roulette -> {
                tab3Fragment?.onRefresh()
            }
            tab4Number -> {
                tab4Fragment?.onRefresh()
            }
            tab5OneTT -> {
                tab5Fragment?.onRefresh()
            }
        }
    }

    fun backFragment() {
        val isBackStack = when (currentTab) {
            tab1Coin -> {
                tab1Fragment?.backStack()
            }
            tab2Dice -> {
                tab2Fragment?.backStack()
            }
            tab3Roulette -> {
                tab3Fragment?.backStack()
            }
            tab4Number -> {
                tab4Fragment?.backStack()
            }
            tab5OneTT -> {
                tab5Fragment?.backStack()
            }
            else -> false
        } ?: false
        if (!isBackStack) {
            supportFragmentManager?.popBackStack()
        }
    }

    private fun setTabColor(tab: String) {
        initAllTabColor()
        when (tab) {
            tab1Coin -> {
                llCoins.setBackgroundColor(R.color.offblue.getColor())
                tvCoins.setTextColor(R.color.white.getColor())
                ivCoins.setImageResource(R.drawable.coin_icon_on)
            }
            tab2Dice -> {
                llDice.setBackgroundColor(R.color.offblue.getColor())
                tvDice.setTextColor(R.color.white.getColor())
                ivDice.setImageResource(R.drawable.dice_icon_on)
            }
            tab3Roulette -> {
                llRoulette.setBackgroundColor(R.color.offblue.getColor())
                tvRoulette.setTextColor(R.color.white.getColor())
                ivRoulette.setImageResource(R.drawable.roulette_icon_on)
            }
            tab4Number -> {
                llNumber.setBackgroundColor(R.color.offblue.getColor())
                tvNumber.setTextColor(R.color.white.getColor())
                ivNumber.setImageResource(R.drawable.number_icon_off)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ivNumber.imageTintList = ColorStateList.valueOf(R.color.white.getColor())
                } else {
                    ivNumber.supportImageTintList = ColorStateList.valueOf(R.color.white.getColor())
                }
            }
            tab5OneTT -> {
                llOTT.setBackgroundColor(R.color.offblue.getColor())
                tvOTT.setTextColor(R.color.white.getColor())
                ivOTT.setImageResource(R.drawable.one_two_three_icon_on)
            }
        }
    }

    private fun initAllTabColor() {
        llCoins.setBackgroundColor(R.color.colorPrimary.getColor())
        tvCoins.setTextColor(R.color.offblue.getColor())
        ivCoins.setImageResource(R.drawable.coin_icon_off)

        llDice.setBackgroundColor(R.color.colorPrimary.getColor())
        tvDice.setTextColor(R.color.offblue.getColor())
        ivDice.setImageResource(R.drawable.dice_icon_off)

        llRoulette.setBackgroundColor(R.color.colorPrimary.getColor())
        tvRoulette.setTextColor(R.color.offblue.getColor())
        ivRoulette.setImageResource(R.drawable.roulette_icon_off)

        llNumber.setBackgroundColor(R.color.colorPrimary.getColor())
        tvNumber.setTextColor(R.color.offblue.getColor())
        ivNumber.setImageResource(R.drawable.number_icon_off)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ivNumber.imageTintList = ColorStateList.valueOf(R.color.offblue.getColor())
        } else {
            ivNumber.supportImageTintList = ColorStateList.valueOf(R.color.offblue.getColor())
        }

        llOTT.setBackgroundColor(R.color.colorPrimary.getColor())
        tvOTT.setTextColor(R.color.offblue.getColor())
        ivOTT.setImageResource(R.drawable.one_two_three_icon_off)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putString("currentTab", currentTab)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            revertFragment(savedInstanceState)
        }
        super.onRestoreInstanceState(savedInstanceState)
    }

    fun revertFragment(state: Bundle) {
        currentTab = state.getString("currentTab") ?: rouletteTab
        tab1Fragment = supportFragmentManager.findFragmentByTag(rouletteTab) as BaseFragment?
        tab2Fragment = supportFragmentManager
            .findFragmentByTag(diceTab) as BaseFragment?
        tab3Fragment = supportFragmentManager.findFragmentByTag(coinTab) as BaseFragment?
        tab4Fragment = supportFragmentManager.findFragmentByTag(numberTab) as BaseFragment?
        tab5Fragment = supportFragmentManager.findFragmentByTag(onettTab) as BaseFragment?
        switchTab(currentTab)
    }
}
