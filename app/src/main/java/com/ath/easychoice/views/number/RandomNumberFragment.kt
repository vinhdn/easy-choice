package com.ath.easychoice.views.number

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams
import com.ath.easychoice.App
import com.ath.easychoice.R
import com.ath.easychoice.common.Constants
import com.ath.easychoice.helper.extensions.*
import com.ath.easychoice.model.TopicResult
import com.ath.easychoice.views.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_random_number.*

class RandomNumberFragment : BaseFragment() {

    private var numberSelected = 1
    private var numberDigits = 1
    private lateinit var listViewImages: List<TextView>
    private lateinit var listLayoutParamsImages: List<LayoutParams>
    private val handler = Handler()
    private val finishRunnable: Runnable = Runnable {
        stopAudio()
        handler.removeCallbacksAndMessages(null)
        val resultDic = TopicResult(
            result.joinToString(separator = ",", limit = numberSelected).replace(",...", ""),
            "",
            App.shared().user?.userId ?: "",
            "",
            isPro = false
        )
        db.collection(Constants.Collection.numberHistories).document().set(resultDic).addOnCompleteListener { }
        mActivity.runOnUiThread {
            llResult.visible()
            result1.text = "${result[0]}"
            result2.text = "${result[1]}"
            result3.text = "${result[2]}"
            result4.text = "${result[3]}"
        }
    }

    private val randomNumberRunnable: Runnable = Runnable { showRandomResult() }

    private val result = arrayOf(99, 99, 99, 99)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_random_number, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listViewImages = listOf(firstImage, secondImage, thirdImage, fourthImage)
        listLayoutParamsImages = listOf(
            firstImage.layoutParams as LayoutParams,
            secondImage.layoutParams as LayoutParams,
            thirdImage.layoutParams as LayoutParams,
            fourthImage.layoutParams as LayoutParams
        )
        listOf(btn1, btn2, btn3, btn4).click { clickView ->
            listOf(btn1, btn2, btn3, btn4).forEach {
                it.setBackgroundResource(0)
                it.setTextColor(R.color.textBlack.getColor())
            }
            clickView.setBackgroundResource(R.drawable.blue_button_2)
            (clickView as? TextView)?.setTextColor(R.color.white.getColor())
            when (clickView.id) {
                R.id.btn1 -> {
                    numberSelected = 1
                }
                R.id.btn2 -> {
                    numberSelected = 2
                }
                R.id.btn3 -> {
                    numberSelected = 3
                }
                R.id.btn4 -> {
                    numberSelected = 4
                }
            }
        }
        listOf(btnDigit1, btnDigit2, btnDigit3, btnDigit4).click { clickView ->
            listOf(btnDigit1, btnDigit2, btnDigit3, btnDigit4).forEach {
                it.setBackgroundResource(0)
                it.setTextColor(R.color.textBlack.getColor())
            }
            clickView.setBackgroundResource(R.drawable.blue_button_2)
            (clickView as? TextView)?.setTextColor(R.color.white.getColor())
            when (clickView.id) {
                R.id.btnDigit1 -> {
                    numberDigits = 1
                }
                R.id.btnDigit2 -> {
                    numberDigits = 2
                }
                R.id.btnDigit3 -> {
                    numberDigits = 3
                }
                R.id.btnDigit4 -> {
                    numberDigits = 4
                }
            }
        }
        btnPlay.click {
            loadCoins(numberSelected)
            val anim = AnimationUtils.loadAnimation(mActivity, R.anim.slide_in_anim)
            anim.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {
                    playRandom()
                }

                override fun onAnimationStart(p0: Animation?) {
                    clPlay.visible()
                }
            })
            clPlay.startAnimation(anim)
        }
        btnNewPlay.click {
            playRandom()
        }
        btnRePlay.click {
            handler.removeCallbacksAndMessages(null)
            val anim = AnimationUtils.loadAnimation(mActivity, R.anim.slide_out_anim)
            anim.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {
                    clPlay.gone()
                }

                override fun onAnimationStart(p0: Animation?) {
                    clPlay.visible()
                }
            })
            clPlay.startAnimation(anim)
        }
        btnHistory.click {
            loadHistories()
        }
    }

    private fun loadHistories() {
        showLoading()
        db.collection(Constants.Collection.numberHistories).whereEqualTo("userId", App.shared().user?.userId).get()
            .addOnSuccessListener { query ->
                val histories = arrayListOf<TopicResult>()
                if (!query.isEmpty) {
                    query.documents.forEach {
                        it.toObject(TopicResult::class.java)?.let { topic ->
                            if (topic.result.isNotEmpty()) {
                                histories.add(topic)
                            }
                        }
                    }
                    histories.sortByDescending { it.datetime }
                    showRandomNumberHistories(mActivity, histories)
                }
                hideLoading()
            }.addOnFailureListener { hideLoading() }
            .addOnCanceledListener { hideLoading() }
    }

    private fun loadCoins(number: Int) {
        result1.visible(number >= 1)
        result2.visible(number >= 2)
        result3.visible(number >= 3)
        result4.visible(number >= 4)

        firstImage.invisible(number < 1)
        secondImage.invisible(number < 2)
        thirdImage.invisible(number < 3)
        fourthImage.invisible(number < 4)
        for (i in 0..3) {
            if (i < 3) {
                listLayoutParamsImages[i].reset()
            }
        }
        if (number == 1) {
            listLayoutParamsImages[0].bottomToBottom = R.id.glHorizontal75
            listLayoutParamsImages[0].topToTop = R.id.glHorizontal25
            listLayoutParamsImages[0].leftToLeft = R.id.glVertical25
            listLayoutParamsImages[0].rightToRight = R.id.glVertical75
        }
        if (number == 2) {
            listLayoutParamsImages[0].bottomToTop = R.id.glHorizontal75
            listLayoutParamsImages[0].topToTop = R.id.glHorizontal25
            listLayoutParamsImages[0].rightToLeft = R.id.glVertical
            listLayoutParamsImages[0].leftToLeft = LayoutParams.PARENT_ID

            listLayoutParamsImages[1].leftToLeft = R.id.glVertical
            listLayoutParamsImages[1].rightToRight = LayoutParams.PARENT_ID
            listLayoutParamsImages[1].bottomToTop = R.id.glHorizontal75
            listLayoutParamsImages[1].topToTop = R.id.glHorizontal25
        }
        if (number >= 3) {
            listLayoutParamsImages[0].bottomToTop = R.id.glHorizontal
            listLayoutParamsImages[0].topToTop = LayoutParams.PARENT_ID
            listLayoutParamsImages[0].rightToLeft = R.id.glVertical
            listLayoutParamsImages[0].leftToLeft = LayoutParams.PARENT_ID

            listLayoutParamsImages[1].leftToLeft = R.id.glVertical
            listLayoutParamsImages[1].rightToRight = LayoutParams.PARENT_ID
            listLayoutParamsImages[1].bottomToTop = R.id.glHorizontal
            listLayoutParamsImages[1].topToTop = LayoutParams.PARENT_ID
            listLayoutParamsImages[2].topToTop = R.id.glHorizontal
            listLayoutParamsImages[2].topToBottom = LayoutParams.PARENT_ID
        }
        if (number == 3) {
            listLayoutParamsImages[2].leftToLeft = R.id.glVertical25
            listLayoutParamsImages[2].rightToLeft = R.id.glVertical75
        }
        if (number == 4) {
            listLayoutParamsImages[2].leftToLeft = LayoutParams.PARENT_ID
            listLayoutParamsImages[2].rightToLeft = R.id.glVertical
        }
        for (i in 0..2) {
            listViewImages[i].layoutParams = listLayoutParamsImages[i]
        }
    }

    private fun playRandom() {
        playAudio(R.raw.number_random, true)
        llResult.invisible()
        handler.removeCallbacksAndMessages(null)
        handler.postDelayed(randomNumberRunnable, 220)
        handler.postDelayed(finishRunnable, 220 * 12)
    }

    private fun showRandomResult() {
        listViewImages.forEachIndexed { index, it ->
            if (index >= numberSelected) return@forEachIndexed
            result[index] = (Math.pow(10.0, numberDigits - 1.0).toInt() until Math.pow(
                10.0,
                numberDigits.toDouble()
            ).toInt()).random()
            it.text = "${result[index]}"
        }
        handler.postDelayed(this@RandomNumberFragment.randomNumberRunnable, 220)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        handler.removeCallbacksAndMessages(null)
    }
}