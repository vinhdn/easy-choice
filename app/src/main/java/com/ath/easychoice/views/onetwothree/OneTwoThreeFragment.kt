package com.ath.easychoice.views.onetwothree

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.ath.easychoice.App
import com.ath.easychoice.R
import com.ath.easychoice.common.Constants
import com.ath.easychoice.helper.extensions.*
import com.ath.easychoice.model.TopicResult
import com.ath.easychoice.views.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_onetwothree.*

class OneTwoThreeFragment : BaseFragment() {

    private var numberSelected = 0
    private val handler = Handler()
    private val finishRunnable: Runnable = Runnable {
        handler.removeCallbacksAndMessages(null)
        val resultDic = TopicResult(
            "$numberSelected,$result",
            "",
            App.shared().user?.userId ?: "",
            "",
            isPro = false
        )
        db.collection(Constants.Collection.ottHistories).document().set(resultDic).addOnCompleteListener { }
        mActivity.runOnUiThread {
            llResult.visible()
            result1.setImageResource(numberSelected.getHand())
            ivYourHand.setImageResource(numberSelected.getHand())
            result2.setImageResource(result.getHand())
            tvResult.visible()
            tvResult.text =
                if (numberSelected == result) {
                    R.string.draw.getString()
                } else if ((numberSelected == 0 && result == 2) ||
                    (numberSelected == 1 && result == 0) ||
                    (numberSelected == 2 && result == 1)
                ) {
                    playAudio(R.raw.win_lose_one_two_three)
                    R.string.you_win.getString()
                } else {
                    R.string.you_lose.getString()
                }
        }
    }

    private val randomNumberRunnable: Runnable = Runnable { showRandomResult() }

    private var result = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_onetwothree, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listOf(cb1, cb2, cb3).click { clickView ->
            listOf(cb1, cb2, cb3).forEach {
                it.setBackgroundResource(R.drawable.blue_button_3)
                it.setImageResource(0)
            }
            clickView.setBackgroundResource(R.drawable.blue_button_2)
            (clickView as? ImageView)?.setImageResource(R.drawable.tick_icon)
            when (clickView.id) {
                R.id.cb1 -> {
                    numberSelected = 0
                }
                R.id.cb2 -> {
                    numberSelected = 1
                }
                R.id.cb3 -> {
                    numberSelected = 2
                }
            }
        }
        btnPlay.click {
            val anim = AnimationUtils.loadAnimation(mActivity, R.anim.slide_in_anim)
            anim.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {
                    playRandom()
                }

                override fun onAnimationStart(p0: Animation?) {
                    clPlay.visible()
                }
            })
            clPlay.startAnimation(anim)
        }
        btnNewPlay.click {
            playRandom()
        }
        btnRePlay.click {
            handler.removeCallbacksAndMessages(null)
            val anim = AnimationUtils.loadAnimation(mActivity, R.anim.slide_out_anim)
            anim.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {
                    clPlay.gone()
                }

                override fun onAnimationStart(p0: Animation?) {
                    clPlay.visible()
                }
            })
            clPlay.startAnimation(anim)
        }
        btnHistory.click {
            loadHistories()
        }
    }

    private fun loadHistories() {
        showLoading()
        db.collection(Constants.Collection.ottHistories).whereEqualTo("userId", App.shared().user?.userId).get()
            .addOnSuccessListener { query ->
                val histories = arrayListOf<TopicResult>()
                if (!query.isEmpty) {
                    query.documents.forEach {
                        it.toObject(TopicResult::class.java)?.let { topic ->
                            if (topic.result.isNotEmpty()) {
                                histories.add(topic)
                            }
                        }
                    }
                    histories.sortByDescending { it.datetime }
                    showOTTHistories(mActivity, histories)
                }
                hideLoading()
            }.addOnFailureListener { hideLoading() }
            .addOnCanceledListener { hideLoading() }
    }

    private fun playRandom() {
        llResult.invisible()
        tvResult.gone()
        handler.removeCallbacksAndMessages(null)
        handler.postDelayed(randomNumberRunnable, 220)
        handler.postDelayed(finishRunnable, 220 * 12)
    }

    private fun showRandomResult() {
        val yourHand = (0..2).random()
        result = (0..2).random()
        ivYourHand.setImageResource(yourHand.getHand())
        ivComputerHand.setImageResource(result.getHand())
        handler.postDelayed(this@OneTwoThreeFragment.randomNumberRunnable, 220)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        handler.removeCallbacksAndMessages(null)
    }
}