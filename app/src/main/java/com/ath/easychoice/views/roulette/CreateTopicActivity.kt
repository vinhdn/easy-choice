package com.ath.easychoice.views.roulette

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ath.easychoice.App
import com.ath.easychoice.R
import com.ath.easychoice.common.Constants
import com.ath.easychoice.helper.extensions.click
import com.ath.easychoice.helper.extensions.inflate
import com.ath.easychoice.helper.extensions.showSelection
import com.ath.easychoice.helper.extensions.textChanges
import com.ath.easychoice.model.OptionModel
import com.ath.easychoice.model.Topic
import com.ath.easychoice.views.base.BaseActivity
import com.google.firebase.firestore.SetOptions
import kotlinx.android.synthetic.main.activity_create_topic.*
import kotlinx.android.synthetic.main.item_create_option.view.*

class CreateTopicActivity: BaseActivity() {

    private var numberOptions = 5
    private var options = hashMapOf<Int, String>()
    private var optionsError = hashMapOf<Int, String?>()
    private var optionsEditText = hashMapOf<Int, EditText?>()
    private var topicId: String? = null
    private var oldTopic: Topic? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_topic)
        topicId = intent.getStringExtra(Constants.Extra.data)
        btnBack.click {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
        btnSelectNumber.click {
            val listData = arrayListOf<Int>()
            for (i in 2..25) {
                listData.add(i)
            }
            showSelection(this, null, listData) {
                numberOptions = it
                btnSelectNumber.text = "$numberOptions"
                rvOptions?.adapter?.notifyDataSetChanged()
            }
        }
        btnCreate.click {
            val topicName = edtTopicName.text?.toString() ?: ""
            if (topicName.isEmpty()) {
                edtTopicName.error = "Nhập tên chủ đề"
                edtTopicName.requestFocus()
                return@click
            }
            for (i in 0 until numberOptions) {
                if (options[i].isNullOrEmpty()) {
                    optionsError[i] = "Nhập lựa chọn"
                    rvOptions.adapter?.notifyItemChanged(i)
                    return@click
                }
            }
            if (App.shared().user == null) return@click
            val topic1 = hashMapOf(
                "isShared" to (oldTopic?.isShared ?: false),
                "likes" to (oldTopic?.likes ?: 0),
                "name" to topicName,
                "nameEn" to topicName,
                "userId" to App.shared().user!!.userId
            )
            val listOptions = mutableListOf<OptionModel>()
            for (i in 0 until numberOptions) {
                val optionStr = options[i] ?: ""
                listOptions.add(OptionModel("id", optionStr, optionStr))
            }
            topic1["options"] = listOptions
            showLoading()
            if (topicId.isNullOrEmpty()) {
                db.collection(Constants.Collection.topics).document().set(topic1, SetOptions.merge()).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        setResult(Activity.RESULT_OK)
                        finish()
                    }
                }
            }else {
                db.collection(Constants.Collection.topics).document(topicId!!).set(topic1, SetOptions.merge()).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        setResult(Activity.RESULT_OK)
                        finish()
                    }
                }
            }
        }
        btnClearName.click {
            edtTopicName.text?.clear()
        }
        rvOptions.layoutManager = LinearLayoutManager(this)
        if (topicId.isNullOrEmpty()) {
            rvOptions.adapter = Adapter()
        } else {
            showLoading()
            db.collection(Constants.Collection.topics)
                .document(topicId!!)
                .get().addOnCompleteListener { task ->
                    hideLoading()
                    if (task.isSuccessful) {
                        val topic = task.result?.toObject(Topic::class.java)
                        if (topic != null) {
                            oldTopic = topic
                            runOnUiThread {
                                tvTopicName.text = "Edit topic"
                                edtTopicName.setText(topic.name)
                                numberOptions = topic.options.size
                                btnSelectNumber.text = "$numberOptions"
                                topic.options.forEachIndexed { index, optionModel ->
                                    options[index] = optionModel.name
                                }
                                rvOptions.adapter = Adapter()
                            }
                        }
                    }
                }
        }
    }

    inner class Adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return Holder(parent.inflate(R.layout.item_create_option))
        }

        override fun getItemCount(): Int {
            return numberOptions
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            holder.itemView.apply {
                edtItemOptionName.tag = position
                edtItemOptionName.setText(options[position])
                val error = optionsError[position]
                if (error?.isNotEmpty() == true) {
                    edtItemOptionName.error = error
                    optionsError[position] = null
                }
                edtItemOptionName.textChanges().subscribe { pair ->
                    (pair.second as? EditText)?.error = null
                    options[pair.second.tag as Int] = pair.first
                }
            }
        }
    }

    inner class Holder(view: View): RecyclerView.ViewHolder(view)
}