package com.ath.easychoice.views.roulette

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ath.easychoice.App
import com.ath.easychoice.R
import com.ath.easychoice.common.Constants
import com.ath.easychoice.helper.extensions.click
import com.ath.easychoice.helper.extensions.inflate
import com.ath.easychoice.helper.extensions.invisible
import com.ath.easychoice.helper.extensions.visible
import com.ath.easychoice.model.Topic
import com.ath.easychoice.model.eventbus.EventSelectedTopic
import com.ath.easychoice.views.base.BaseActivity
import kotlinx.android.synthetic.main.activity_list_roulete.*
import kotlinx.android.synthetic.main.item_topic.view.*
import org.greenrobot.eventbus.EventBus

class ListRouletteActivity : BaseActivity() {

    val topics = arrayListOf<Topic>()
    private var isEditting = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_roulete)
        btnBack.click {
            finish()
        }
        btnEdit.click {
            btnEditClick()
        }
        btnAdd.click {
            val intent = Intent(this, CreateTopicActivity::class.java)
            startActivityForResult(intent, 123)
        }
        rvTopics.layoutManager = LinearLayoutManager(this)
        loadTopics()
    }

    private fun btnEditClick() {
        btnEdit.setImageResource(if (isEditting) R.drawable.edit_icon else R.drawable.tick_icon)
        isEditting = !isEditting
        rvTopics?.adapter?.notifyDataSetChanged()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            loadTopics()
        }
    }

    private fun loadTopics() {
        showLoading()
        db.collection(Constants.Collection.topics).whereEqualTo("userId", App.shared().user?.userId).get()
            .addOnSuccessListener { query ->
                if (!query.isEmpty) {
                    topics.clear()
                    query.documents.forEach {
                        it.toObject(Topic::class.java)?.let { topic ->
                            topic.id = it.id
                            topics.add(topic)
                        }
                    }
                }
                topics.sortBy { it.timeUpdate }
                runOnUiThread {
                    hideLoading()
                    rvTopics.adapter = Adapter()
                }
            }.addOnFailureListener { hideLoading() }
            .addOnCanceledListener { hideLoading() }
    }

    inner class Adapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return Holder(parent.inflate(R.layout.item_topic))
        }

        override fun getItemCount(): Int {
            return topics.size
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            holder.itemView.apply {
                val topic = topics[position]
                btnDelete.invisible(!isEditting)
                btnEdit.invisible(!isEditting)
                llNumber.visible(!isEditting)
                tvItemTopicName.text = topic.name
                click {
                    if (!isEditting) {
                        EventBus.getDefault().post(EventSelectedTopic(topic))
                        setResult(Activity.RESULT_OK)
                        finish()
                    }
                }
                btnDelete.click {
                    db.collection(Constants.Collection.topics).document(topic.id).delete()
                        .addOnCompleteListener {
                            if (topics.remove(topic)) {
                                notifyItemRemoved(position)
                            }
                        }
                }
                btnEdit.click {
                    btnEditClick()
                    val intent = Intent(this@ListRouletteActivity, CreateTopicActivity::class.java)
                    intent.putExtra(Constants.Extra.data, topic.id)
                    startActivityForResult(intent, 124)
                }
            }
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view)
}