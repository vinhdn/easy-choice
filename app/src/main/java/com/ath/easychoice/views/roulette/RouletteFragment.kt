package com.ath.easychoice.views.roulette

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ath.easychoice.App
import com.ath.easychoice.model.OptionModel
import com.ath.easychoice.R
import com.ath.easychoice.common.Constants
import com.ath.easychoice.helper.extensions.click
import com.ath.easychoice.helper.extensions.showRouletteHistories
import com.ath.easychoice.model.Topic
import com.ath.easychoice.model.TopicResult
import com.ath.easychoice.model.eventbus.EventSelectedTopic
import com.ath.easychoice.views.base.BaseFragment
import com.ath.easychoice.widget.SpinningWheelView
import kotlinx.android.synthetic.main.fragment_roulette.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import kotlin.random.Random

class RouletteFragment : BaseFragment() {

    val topics = arrayListOf<Topic>()
    var topicSelected: Topic? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_roulette, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        EventBus.getDefault().register(this)
        btnRoulette.click {
            roulette.rotate(50f, 5000 + (360f * (Random(100).nextFloat() / 100f)).toLong(), 50)
        }
        btnEdit.click {
            val intent = Intent(activity, ListRouletteActivity::class.java)
            startActivityForResult(intent, 123)
        }
        btnHistory.click {
            loadHistories()
        }
        tvSelected.text = ""
        roulette.isEnabled = false
        roulette.items = listOf(
            OptionModel("123", "Buffet"),
            OptionModel("123", "Hải sản"),
            OptionModel("123", "Đồ Tầu"),
            OptionModel("123", "Bít tết"),
            OptionModel("123", "Pizza")
        )
        roulette.onRotationListener = object : SpinningWheelView.OnRotationListener<OptionModel> {
            override fun onRotation() {
                tvSelected.text = "Selecting..."
                playAudio(R.raw.roulette, true)
            }

            override fun onStopRotation(item: OptionModel) {
                playAudio(R.raw.noti_resuld_roulette)
                tvSelected.text = item.name
                if (topicSelected != null) {
                    val result = TopicResult(item.name, topicSelected!!.id, topicSelected!!.userId, topicSelected!!.name)
                    db.collection(Constants.Collection.topicHistories).document().set(result).addOnCompleteListener {  }
                }
            }
        }
        showLoading()
        db.collection(Constants.Collection.topics).whereEqualTo("userId", App.shared().user?.userId).get().addOnSuccessListener { query ->
            if(!query.isEmpty) {
                query.documents.forEach {
                    it.toObject(Topic::class.java)?.let { topic ->
                        topic.id = it.id
                        topics.add(topic)
                    }
                }
                topicSelected = topics[0]
                activity?.runOnUiThread {
                    hideLoading()
                    roulette.items = topicSelected!!.options
                    tvTopicName.text = topicSelected!!.name
                }
            } else {
                hideLoading()
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onTopicSelected(event: EventSelectedTopic) {
        topicSelected = event.topic
        roulette.items = topicSelected!!.options
        tvTopicName.text = topicSelected!!.name
    }

    override fun onDestroyView() {
        super.onDestroyView()
        EventBus.getDefault().unregister(this)
    }

    private fun loadHistories() {
        showLoading()
        db.collection(Constants.Collection.topicHistories).whereEqualTo("userId", App.shared().user?.userId).get().addOnSuccessListener { query ->
            val histories = arrayListOf<TopicResult>()
            if(!query.isEmpty) {
                query.documents.forEach {
                    it.toObject(TopicResult::class.java)?.let { topic ->
                        histories.add(topic)
                    }
                }
                histories.sortByDescending { it.datetime }
                showRouletteHistories(mActivity, histories)
            }
            hideLoading()
        }.addOnFailureListener { hideLoading() }
            .addOnCanceledListener { hideLoading() }
    }
}